/**
 * Copyright (c) 2024 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import {
  AbilityAware,
  AbilityPluginBinding,
  FlutterPlugin,
  FlutterPluginBinding,
} from '@ohos/flutter_ohos';
import { MethodCallHandlerImpl } from './MethodCallHandlerImpl';
import { UIAbility } from '@kit.AbilityKit';

const TAG: string = '【FlutterBarcodeScannerOhosPlugin】';

export default class FlutterBarcodeScannerOhosPlugin implements FlutterPlugin, AbilityAware {
  private flutterPluginBinding: FlutterPluginBinding | null = null;
  private methodCallHandler: MethodCallHandlerImpl | null = null;
  private ability: UIAbility | null = null;
  private context: Context | null = null;

  constructor() {
  }

  getUniqueClassName(): string {
    return "FlutterBarcodeScannerOhosPlugin"
  }

  onAttachedToEngine(binding: FlutterPluginBinding): void {
    this.flutterPluginBinding = binding;
  }

  onDetachedFromEngine(binding: FlutterPluginBinding): void {
    this.flutterPluginBinding = null;
  }

  onAttachedToAbility(binding: AbilityPluginBinding): void {
    if (this.flutterPluginBinding != null) {
      this.ability = binding.getAbility();
      this.context = this.ability.context;
      this.methodCallHandler = new MethodCallHandlerImpl(this.context, this.flutterPluginBinding.getBinaryMessenger());
    }
  }

  onDetachedFromAbility(): void {
    if (this.methodCallHandler != null) {
      this.methodCallHandler.stopListening();
      this.methodCallHandler = null;
    }
  }
}